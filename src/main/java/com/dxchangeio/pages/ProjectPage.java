package com.dxchangeio.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dxchangeio.Base.BaseTest;

public class ProjectPage extends BaseTest {
	public static ProjectPage projectPage;
	@FindBy(xpath="//span//a[contains(text(), 'Home')]")
	WebElement clickOnHome;
	public ProjectPage()
	{
		PageFactory.initElements(driver, this);
	}
	
public void validateHomeLink() throws InterruptedException
{
	Thread.sleep(3000);
	clickOnHome.click();
}
}
