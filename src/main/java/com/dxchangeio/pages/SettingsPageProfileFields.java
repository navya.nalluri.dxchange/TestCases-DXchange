package com.dxchangeio.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dxchangeio.Base.BaseTest;

public class SettingsPageProfileFields extends BaseTest {
	public static SettingsPageProfileFields settingsPageProfileFields;
	
	@FindBy(id = "firstName")
	WebElement enterFName;
	
	@FindBy(id="lastName")
	WebElement enterLastName;
	
	/*@FindBy(xpath="//label//span[@class='ant-radio ant-radio-checked']")
	WebElement clckOnGender;*/
	
	@FindBy(xpath="//button[@class='ant-btn ant-btn-primary']")
	WebElement clckOnUpadateProfile;
	
	@FindBy(xpath="//button[@class='ant-btn ant-btn-default']")
	WebElement clckOnChngPswd;
	
	@FindBy(id = "oldPassword")
	WebElement enterOldPswd;
	
	@FindBy(id = "newPassword")
	WebElement enterNewPswd;
	
	@FindBy(id = "confirmNewPassword")
	WebElement enteronfmNewPswd;
	
	@FindBy(xpath = "//button[@class='ant-btn ant-btn-primary']")
	WebElement clckOnUpdatePswd;
	
	@FindBy(xpath = "//button[@class='ant-btn ant-btn-primary ng-star-inserted']")
	WebElement clckOnGoBack;
	
	public SettingsPageProfileFields()
	{
		PageFactory.initElements(driver, this);
		
	}
 public void validateProfile(String fName,String lName) throws InterruptedException
 {
	 Thread.sleep(3000);
	 enterFName.clear();
	 enterFName.sendKeys(fName);
	 enterLastName.clear();
	 enterLastName.sendKeys(lName);
	 /*clckOnGender.click();*/
	 clckOnUpadateProfile.click();
	 
 }

 public void validateChngPswd(String oldPswd,String newPswd,String cnfmPswd) throws InterruptedException
 {
	 clckOnChngPswd.click();
	 enterOldPswd.sendKeys(oldPswd);
	 enterNewPswd.sendKeys(newPswd);
	 enteronfmNewPswd.sendKeys(cnfmPswd);
	 clckOnUpdatePswd.click();
	 Thread.sleep(3000);
	 clckOnGoBack.click();
 }
	
}
