package com.dxchangeio.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dxchangeio.Base.BaseTest;

public class ManageCenterDashboard extends BaseTest {
	public static ManageCenterDashboard manageCenterDashboard;
	
	@FindBy(xpath="//div[@class='content__title']")
	WebElement manageTextDashboard;
	
	@FindBy(xpath="//h4[contains(text(),'Permissions')]")
	WebElement clckOnPermissions;
	
	@FindBy(xpath="//h4[contains(text(),'Events')]")
	WebElement clckOnEvents;
	
	@FindBy(xpath="//h4[contains(text(),'Deployments')]")
	WebElement clckOnDeploy;
	
	@FindBy(xpath="//h4[contains(text(),'Organisations')]")
	WebElement clckOnOrganize;
	
	

	
	public ManageCenterDashboard()
	{
		PageFactory.initElements(driver, this);
	}
	public String validateManageText()
	{
	System.out.println(manageTextDashboard.getText());
	
	return manageTextDashboard.getText();
	}
	public void validatePermissionsLink()
	{
		clckOnPermissions.click();
	}
	public void validateEventsLink()
	{
		clckOnEvents.click();
	}
	public void validateDeploymentsLink()
	{
		clckOnDeploy.click();
	}
	public void validateOrganizations()
	{
		clckOnOrganize.click();
	}

}
