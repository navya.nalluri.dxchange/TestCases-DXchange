package com.dxchangeio.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dxchangeio.Base.BaseTest;

public class PartnersPage extends BaseTest{
	public static PartnersPage partnersPage;
	@FindBy(xpath="//button[@class='ant-btn ant-btn-dashed ng-star-inserted']")
	WebElement clkCreateBtn;
public PartnersPage()
{
	PageFactory.initElements(driver, this);
}
public void validatecreateBtn() throws InterruptedException
{
	Thread.sleep(3000);
	clkCreateBtn.click();
}
}
