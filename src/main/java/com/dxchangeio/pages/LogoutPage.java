package com.dxchangeio.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dxchangeio.Base.BaseTest;

public class LogoutPage extends BaseTest{
	public static LogoutPage logoutPage;
	@FindBy(xpath="//div[@class='item d-flex align-items-center px-sm ant-dropdown-trigger']")
	WebElement HarishLink;
	@FindBy(xpath="//div//div//div//div//div//div//i[@class='anticon anticon-logout mr-sm']")
	WebElement clkLogoutBtn;
	public LogoutPage()
	{
	PageFactory.initElements(driver, this);
	}
	public void moveToHarishLink()
	{
		Actions action= new Actions(driver);
		action.moveToElement(HarishLink).build().perform();
		clkLogoutBtn.click();
	}
	

}
