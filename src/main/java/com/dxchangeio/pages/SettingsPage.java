package com.dxchangeio.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dxchangeio.Base.BaseTest;

public class SettingsPage extends BaseTest {
	public static LoginPage loginPage;
	SettingsPage settingsPage;
	@FindBy(xpath="//div[@class='item d-flex align-items-center px-sm ant-dropdown-trigger']")
	WebElement HarishLink;
	@FindBy(xpath="//div//i[@class='anticon anticon-setting mr-sm']")
    WebElement clkOnSettingLink;
	public SettingsPage()
	{
		PageFactory.initElements(driver, this);
	}
	public void moveToSettingLink()
	{
		Actions action= new Actions(driver);
		action.moveToElement(HarishLink).build().perform();
		clkOnSettingLink.click();
	}
	
	
}
