package com.dxchangeio.pages;

import java.util.Set;

import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;
//import org.openqa.selenium.support.ui.WebDriverWait;

import com.dxchangeio.Base.BaseTest;

public class LoginPage extends BaseTest {
	public static LoginPage loginPage;
	//public static WebDriverWait wait;

	@FindBy(xpath = "//span[@class='title']")
	WebElement getTitle;
	
	@FindBy(xpath = "//div[@class='desc']")
	WebElement getTxt;
	
	@FindBy(xpath = "//input[@formcontrolname='email']")
	WebElement emailTxt;
	
	@FindBy(xpath = "//input[@formcontrolname='password']")
	WebElement pswdTxt;
	
	@FindBy(xpath = "//button[@type='submit']")
	WebElement loginBtn;
	
	@FindBy(xpath = "//a[@routerlink='/forgotpassword'  and  @class='forgot']")
	WebElement clkForgotPswd;
	
	@FindBy(xpath = "//a[@href='http://dxchange.io' and @target='_blank']")
	WebElement clkDXchangeLink;

	public LoginPage() 
	{
		PageFactory.initElements(driver, this);
	}

	public void validateLogin(String email, String password) 
	{
		emailTxt.sendKeys(email);
		pswdTxt.sendKeys(password);
		loginBtn.click();
		System.out.println("Successfully loggedin");
	}
	public  void flash()
	{
		System.out.println(loginBtn);
		JavascriptExecutor js =((JavascriptExecutor)driver);
		String bgcolor = loginBtn.getCssValue("backgroundColor");
		for(int i = 0; i < 10; i++)
		{
			changeColor("rgb(255,255,42)",loginBtn,driver);
			changeColor(bgcolor,loginBtn,driver);
			
		}
	}
	
	public  void changeColor(String color,WebElement loginBtn, WebDriver driver)
	{
		JavascriptExecutor js=((JavascriptExecutor)driver);
		js.executeScript("arguments[0].style.backgroundColor='"+color+"'", loginBtn);
	try {
		Thread.sleep(20);
	}
	catch(InterruptedException e) 
	{
	}
	}

	public String validateTitleTxt() 
	{
		System.out.println("Displayed Successfully");
		return getTitle.getText();
	}

	public String validateDTXTxt() {
		System.out.println("DTX displayed Successfully");
		return getTxt.getText();
	}

	public void ClickOnDXchnge() throws InterruptedException 
	{
		String parentWindow = driver.getWindowHandle();
		System.out.println("parent window id: " + parentWindow);
		System.out.println("clickedOn succesfully");
		clkDXchangeLink.click();
		Thread.sleep(3000);
		Set<String> allWindows = driver.getWindowHandles();
	   int windowSize =	allWindows.size();
	   System.out.println("Number of windows opened by selenium:"+windowSize);
	   int i=1;
	   for(String window:allWindows)
	   {
		   System.out.println("window id : "+i+"" +window);
		   i++;
		   if(!parentWindow.equalsIgnoreCase(window))
		   {
			   driver.switchTo().window(window);
			  Thread.sleep(2000);
			  driver.close();
		   }
		  }
	   driver.switchTo().window(parentWindow);
	 
	}

	public void clickOnForget() 
	{
		try {
			Thread.sleep(3000);
			clkForgotPswd.click();
		} catch (InterruptedException e) {
			e.printStackTrace();
		}

		/*
		 * wait= new WebDriverWait(driver,50);
		 * wait.until(ExpectedConditions.invisibilityOfElementLocated(By.
		 * xpath("//a[@class['forgot'] and contains(text(),'Forgot password')]")));
		 */

	}

}
