package com.dxchangeio.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dxchangeio.Base.BaseTest;

public class AgreementsPage extends BaseTest {

	public static AgreementsPage agreementsPage;
	@FindBy(xpath="//i[@class='anticon anticon-plus ng-star-inserted']")
	WebElement createButton;
	 
	public AgreementsPage()
	{
		PageFactory.initElements(driver,this);
	}
	public void validateCreateBtn() throws InterruptedException
	{
		Thread.sleep(3000);
		createButton.click();
	}
}
