package com.dxchangeio.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dxchangeio.Base.BaseTest;

public class EndpointsPage extends BaseTest{
	public static EndpointsPage endPointsPage;
	@FindBy(xpath="//button//i[@class='anticon anticon-bars ng-star-inserted']")
	WebElement clkOnIcon;
	public EndpointsPage()
	{
		PageFactory.initElements(driver, this);
		
	}
	public void validateIcon()
	{
		clkOnIcon.click();
	}

}
