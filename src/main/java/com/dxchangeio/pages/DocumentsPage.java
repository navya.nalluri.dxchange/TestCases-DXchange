package com.dxchangeio.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dxchangeio.Base.BaseTest;

public class DocumentsPage extends BaseTest{
	public static DocumentsPage documentsPage;
	@FindBy(xpath="//button[@routerlink='/resources/edit-document']")
	WebElement  clkCreateBtn;
	public DocumentsPage()
	{
		PageFactory.initElements(driver,this);
	}
	public void validateCreateBtn()
	{
		clkCreateBtn.click();
	}
	

}
