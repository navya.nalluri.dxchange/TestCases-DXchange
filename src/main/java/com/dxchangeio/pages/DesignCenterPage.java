package com.dxchangeio.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dxchangeio.Base.BaseTest;

public class DesignCenterPage extends BaseTest {
	public static DesignCenterPage designCenterPage;
	
	@FindBy(xpath="//small[contains(text(), 'Welcome !')]")
	WebElement welcomeTxt;
	@FindBy(xpath="//app-dashboard[@class='ng-star-inserted']")
	WebElement dashboardTxt;
	
	@FindBy(xpath = "//h4[@class='h4' and contains(text(), 'Projects')]")
	WebElement clickOnProjects;
	@FindBy(xpath = "//h4[contains(text(),'Endpoints')]")
	WebElement clickOnEndPoints;
	@FindBy(xpath = "//h4[contains(text(),'Documents')]")
	WebElement clickOnDocuments;
	@FindBy(xpath = "//h4[contains(text(),'Partners')]")
	WebElement clickOnPartners;
	@FindBy(xpath = "//h4[contains(text(),'Agreements')]")
	WebElement clickOnAgreements;
	@FindBy(xpath = "//h4[contains(text(),'Document Formats')]")
	WebElement clickOnDocumentFormats;

	@FindBy(xpath = "//div[contains(text(),'Manage Projects')]")
	WebElement manageProjectsTxt;
	@FindBy(xpath = "//div[contains(text(),'Manage Endpoints')]")
	WebElement manageEndPointsTxt;
	@FindBy(xpath = "//div[contains(text(),'Manage Documents')]")
	WebElement manageDocumentsTxt;
	@FindBy(xpath = "//div[contains(text(),'Manage Partners')]")
	WebElement managePartnersTxt;
	@FindBy(xpath = "//div[contains(text(),'Manage Agreements')]")
	WebElement manageAgreementsTxt;
	@FindBy(xpath = "//div[contains(text(),'Manage DocFormats')]")
	WebElement manageDocumentFormatsTxt;
	
	
public DesignCenterPage() {
		PageFactory.initElements(driver, this);
	}
	
	public String verifyWelcomeTxt()
	{
		return welcomeTxt.getText();
	}
	
	public String verifyDashboardTxt()
	{
		 return dashboardTxt.getText();
	}

	public void verifyProjectsLink() throws InterruptedException {
		Thread.sleep(3000);
		clickOnProjects.click();
		System.out.println("Test Passed,Entered to Projects platform");
	}

	public String ManageProjectsTxt() {
		return manageProjectsTxt.getText();
	}

	public void verifyEndPointsLink() {
		clickOnEndPoints.click();
		System.out.println("Test Passed,Entered to EndPoints platform");

	}

	public String ManageEndPointsTxt() {
		return manageEndPointsTxt.getText();
	}

	public void verifyDocumentsLink() {
		clickOnDocuments.click();
		System.out.println("Test Passed,Entered to Documents platform");

	}

	public String ManageDocumentsTxt() {
		return manageDocumentsTxt.getText();

	}

	public void verifyPartnersLink() {
		clickOnPartners.click();
		System.out.println("Test Passed,Entered to Partners platform");

	}

	public String ManagePartnersTxt() {
		return managePartnersTxt.getText();

	}

	public void verifyAgreementsLink() {
		clickOnAgreements.click();
		System.out.println("Test Passed,Entered to Agreements platform");

	}

	public String AgreementsTxt() {
		return manageAgreementsTxt.getText();

	}

	public void verifyDocumentFormatsLink() {
		clickOnDocumentFormats.click();
		System.out.println("Test Passed,Entered to DocumentFormats platform");

	}

	public String DocumentFormatTxt() {
		return manageDocumentFormatsTxt.getText();

	}

}
