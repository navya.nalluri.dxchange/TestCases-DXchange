package com.dxchangeio.pages;

import org.openqa.selenium.WebElement;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dxchangeio.Base.BaseTest;

public class HomePage extends BaseTest {
	public static HomePage homePage;
	
	public static ManageCenterDashboard manageCenterDashboard;
	
	@FindBy(xpath="//span[contains(text(),'DXchange Platform')]")
	WebElement homePageTitle;
	@FindBy(xpath="//h3[contains(text(),'Design Center')]")
	WebElement clkOnDesignCenter;
	
	@FindBy(xpath = "//h3[contains(text(), 'Manage Center')]")
	WebElement clckOnManageCenter;
	
	
	public HomePage()
	{
		PageFactory.initElements(driver, this);
	}
	public String getHomePageTitle() throws InterruptedException
	{
		System.out.println("Title Succesfully Displayed");
		Thread.sleep(3000);
		return homePageTitle.getText();
	}
	public void verifyDesignCenterTxt()
	{
		clkOnDesignCenter.click();
	}
	public  void validateManageCenter()
	{
		manageCenterDashboard = new ManageCenterDashboard();
		clckOnManageCenter.click();
		
	}

}
