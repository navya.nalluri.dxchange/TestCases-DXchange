package com.dxchangeio.pages;

import java.util.ArrayList;
import java.util.List;

import org.openqa.selenium.By;
import org.openqa.selenium.JavascriptExecutor;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.interactions.Actions;
import org.openqa.selenium.support.FindBy;
import org.openqa.selenium.support.PageFactory;

import com.dxchangeio.Base.BaseTest;

public class SettingsUserPrefrencesPage extends BaseTest{
	public static WebDriver driver;
	
	@FindBy(xpath="/html/body/app-root/layout-default/div/section/app-extras-settings/nz-row/nz-col[1]/nz-card[1]/div[2]/a[2]")
     WebElement EnterUsrPrefre;
	
	@FindBy(xpath="//input[@class='ant-select-search__field ng-tns-c16-34 ng-pristine ng-valid ng-star-inserted ng-touched']")
	WebElement EnterDefaultEnviron;
	
	@FindBy(xpath="//input[@class='ant-select-search__field ng-tns-c16-35 ng-pristine ng-valid ng-star-inserted ng-touched']")
	WebElement EnterLandingPage;
	
	@FindBy(xpath="//input[@class='ant-select-search__field ng-tns-c16-36 ng-pristine ng-valid ng-star-inserted ng-touched']")
	WebElement EnterLang;
	
	@FindBy(xpath="//input[@class='ant-select-search__field ng-tns-c16-37 ng-untouched ng-pristine ng-valid ng-star-inserted']")
	WebElement EnterNoOfItems;
	
	@FindBy(xpath="//button//span[contains(text(),'Update')]")
	WebElement clckOnUpdate;
	
	public SettingsUserPrefrencesPage()
	{
		PageFactory.initElements(driver, this);
	}
    public void validateEnivornment() throws InterruptedException  
    {
//    	EnterUsrPrefre.click();
    	Thread.sleep(3000);
    	Actions builder = new Actions(driver);
    	builder.moveToElement(EnterUsrPrefre).click(EnterUsrPrefre);
    	builder.perform();
    	/*clickOnUserPreferences(EnterUsrPrefre, driver);
   	EnterDefaultEnviron.click();
    EnterDefaultEnviron.clear();*/
    	 
    		
    /*	System.out.println(list.size());
  	for(int i=0; i<list.size(); i++) {
			//System.out.println(list.get(i).getText());
			if(list.get(i).getText().equals("dxchange1234"));
			list.get(i).click();
			System.out.println("Successfully clicked");
			break;
    }*/
    }
    
    	public void clickOnUserPreferences(WebElement element,WebDriver driver) {
    		JavascriptExecutor js = ((JavascriptExecutor)driver);
    		System.out.println(element);
    		js.executeScript("arguments[0].click();", element);
    	}
    	
    }
	
	

