package com.dxchangeio.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dxchangeio.Base.BaseTest;
import com.dxchangeio.pages.LoginPage;
import com.dxchangeio.pages.SettingsPage;
import com.dxchangeio.pages.SettingsPageProfileFields;

public class SettingsPageProfileFieldsTest extends BaseTest {
	public static LoginPage loginPage;
	public static SettingsPage settingsPage;
	public static SettingsPageProfileFields settingsPageProfileFields;

	@BeforeMethod
	public void setUp() {
		initialize();
		openBrowser();
		loginPage = new LoginPage();
		loginPage.validateLogin(prop.getProperty("email"), prop.getProperty("password"));
		System.out.println("Test passed");
		settingsPage = new SettingsPage();
		settingsPage.moveToSettingLink();
		System.out.println("SettingsLink is Displayed");
	}
	@Test(priority = 1)
	public void verifyProfileDetails() throws InterruptedException
	{
		settingsPageProfileFields=new SettingsPageProfileFields();
		settingsPageProfileFields.validateProfile(prop.getProperty("fName"), prop.getProperty("lName"));
		System.out.println("Profile Updated Successfully");
	}
	
	@Test(priority = 2)
	public void verifyChngPswdDetails() throws InterruptedException
	{
		settingsPageProfileFields=new SettingsPageProfileFields();
		settingsPageProfileFields.validateChngPswd(prop.getProperty("oldPswd"), prop.getProperty("newPswd"),  prop.getProperty("cnfmPswd"));
		System.out.println("Password changed Successfully");
	}
	@AfterMethod
	public void close()
	{
		driver.quit();
	}

}
