package com.dxchangeio.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dxchangeio.Base.BaseTest;
import com.dxchangeio.pages.DesignCenterPage;
import com.dxchangeio.pages.DocumentsPage;
import com.dxchangeio.pages.HomePage;
import com.dxchangeio.pages.LoginPage;

public class DocumentsPageTest extends BaseTest{
	public static LoginPage loginPage;
	public static HomePage homePage;
	public static DesignCenterPage designCenterPage;
	public static DocumentsPage documentsPage;
	
	@BeforeMethod
	public void setUp()
	{
		initialize();
		openBrowser();
		loginPage = new LoginPage();
		loginPage.validateLogin(prop.getProperty("email"), prop.getProperty("password"));
		homePage= new HomePage();
		homePage.verifyDesignCenterTxt();
		designCenterPage= new DesignCenterPage();
		designCenterPage.verifyDocumentsLink();
	}
	@Test
	public void clkCreateLink()
	{
		documentsPage= new DocumentsPage();
		documentsPage.validateCreateBtn();
	}
	@AfterMethod
	public void close()
	{
		driver.quit();
	}

}
