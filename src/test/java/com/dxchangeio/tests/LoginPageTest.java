package com.dxchangeio.tests;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;

import org.testng.annotations.Test;

import com.dxchangeio.Base.BaseTest;
import com.dxchangeio.pages.LoginPage;

public class LoginPageTest extends BaseTest 
{
	public static LoginPage loginPage;

	@BeforeMethod
	public void setUp() {
		initialize();
		openBrowser();
	}

	@Test(priority = 1)
	public void validateLoginTitle() 
	{
		loginPage = new LoginPage();
		String getTitle = loginPage.validateTitleTxt();
		Assert.assertEquals(getTitle, "DXchange.io");

	}

	@Test(priority = 2)
	public void verifyDtxTxt() 
	{
		loginPage = new LoginPage();
		String getText = loginPage.validateDTXTxt();
		Assert.assertEquals(getText, "Digital Transformation Xchange");
	}

	@Test(priority = 3)
	public static void ValidateLoginPage() 
	{

		loginPage = new LoginPage();
		loginPage.flash();
		loginPage.validateLogin(prop.getProperty("email"), prop.getProperty("password"));
		System.out.println("Test passed");
	}

	@Test(priority = 4)
	public void verifyForgetLink() 
	{
		loginPage = new LoginPage();
		loginPage.clickOnForget();
		System.out.println("Test Passed");

	}
	
	@Test(priority = 5)
	public void clickDxchangeLink() throws InterruptedException 
	{
		loginPage = new LoginPage();
		Thread.sleep(3000);
		loginPage.ClickOnDXchnge();
	}

	@AfterMethod
	public void close()
	{
		driver.quit();
	}
}
