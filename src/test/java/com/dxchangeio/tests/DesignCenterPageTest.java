package com.dxchangeio.tests;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dxchangeio.Base.BaseTest;
import com.dxchangeio.pages.DesignCenterPage;
import com.dxchangeio.pages.HomePage;
import com.dxchangeio.pages.LoginPage;

public class DesignCenterPageTest extends BaseTest {
	public static DesignCenterPage designCenterPage;
	public static LoginPage loginPage;
	public static HomePage homePage;

	@BeforeMethod
	public void setUp() {
		initialize();
		openBrowser();
		loginPage = new LoginPage();
		/*System.out.println(prop.getProperty("browser"));
		System.out.println(prop.getProperty("email"));*/
		loginPage.validateLogin(prop.getProperty("email"), prop.getProperty("password"));
		/*System.out.println("Test passed");*/
		homePage = new HomePage();
		homePage.verifyDesignCenterTxt();
		System.out.println("Successfully entered to Design Center Platform");
	}
	
	@Test(priority = 1)
	public void validateWelcomeTxt()
	{
		designCenterPage=new DesignCenterPage();
		String getText=designCenterPage.verifyWelcomeTxt();
		Assert.assertEquals(getText, "Welcome !");
		System.out.println("Welcome text displayed successfully");
	}
	/*@Test(priority = 2)
	public void validateDahboardtxt() throws InterruptedException
	{
		designCenterPage= new DesignCenterPage();
		String getText=designCenterPage.verifyDashboardTxt();
		Thread.sleep(3000);
		Assert.assertEquals(getText, "Dashboard");
		System.out.println("Dashboard text displayed successfully");
	}*/

	@Test(priority = 2)
	public void validateProjectsLink() throws InterruptedException {
		designCenterPage = new DesignCenterPage();
		designCenterPage.verifyProjectsLink();
		String getTxt=designCenterPage.ManageProjectsTxt();
		Assert.assertEquals(getTxt,"Manage Projects");
	}
	@Test(priority = 3)
	public void validateEndPointsLink() {
		designCenterPage = new DesignCenterPage();
		designCenterPage.verifyEndPointsLink();
		String getTxt=designCenterPage.ManageEndPointsTxt();
		Assert.assertEquals(getTxt, "Manage Endpoints");
	}
	@Test(priority = 4)
	public void validateDocumentsLink() {
		designCenterPage = new DesignCenterPage();
		designCenterPage.verifyDocumentsLink();
		String getTxt=designCenterPage.ManageDocumentsTxt();
		Assert.assertEquals(getTxt, "Manage Documents");
	}
	@Test(priority = 5)
	public void validatePartnersLink() {
		designCenterPage = new DesignCenterPage();
		designCenterPage.verifyPartnersLink();
		String getTxt=designCenterPage.ManagePartnersTxt();
		Assert.assertEquals(getTxt, "Manage Partners");
	}
	@Test(priority = 6)
	public void validateAgreementsLink() {
		designCenterPage = new DesignCenterPage();
		designCenterPage.verifyAgreementsLink();
		String getTxt=designCenterPage.AgreementsTxt();
		Assert.assertEquals(getTxt, "Manage Agreements");
	}
	@Test(priority = 7)
	public void validateDocumentFormatsLink() {
		designCenterPage = new DesignCenterPage();
		designCenterPage.verifyDocumentFormatsLink();
		String getTxt=designCenterPage.DocumentFormatTxt();
		Assert.assertEquals(getTxt, "Manage DocFormats");
	}

	@AfterMethod
	public void close() {
		driver.quit();
	}

}
