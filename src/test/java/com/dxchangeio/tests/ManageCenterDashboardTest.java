package com.dxchangeio.tests;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dxchangeio.Base.BaseTest;
import com.dxchangeio.pages.HomePage;
import com.dxchangeio.pages.LoginPage;
import com.dxchangeio.pages.ManageCenterDashboard;



public class ManageCenterDashboardTest extends BaseTest{
	public static ManageCenterDashboard manageCenterDashboard;
	public static LoginPage loginPage;
	public static HomePage homePage;
	
	@BeforeMethod
	public void setUp()
	{
		initialize();
		openBrowser();
	
		loginPage = new LoginPage();
		loginPage.validateLogin(prop.getProperty("email"), prop.getProperty("password"));
		homePage = new HomePage();
		homePage.validateManageCenter();
		System.out.println("Successfully entered to Manage Center Platform");
	}
	@Test(priority = 1)
	public void verifyManageText()
	{
		manageCenterDashboard = new ManageCenterDashboard();
		 String getText = manageCenterDashboard.validateManageText();
		Assert.assertEquals(getText, "Manage "
				+ "Dashboard");
		manageCenterDashboard.validateManageText();
		System.out.println("Manage Text displayed successfully");
		

	}
	@Test(priority = 2)
	public void verifyPermissionsLink()
	{
		manageCenterDashboard = new ManageCenterDashboard();
		manageCenterDashboard.validatePermissionsLink();
		
	}
	@Test(priority = 3)

public void verifyEventsLink()
{
	manageCenterDashboard = new ManageCenterDashboard();
	manageCenterDashboard.validateEventsLink();
}
	@Test(priority = 4)

public void verifyDeploymentsLink()
{
	manageCenterDashboard = new ManageCenterDashboard();
	manageCenterDashboard.validateDeploymentsLink();
}
	@Test(priority = 5)

public void verifyOrganizationsLink()
{
	manageCenterDashboard = new ManageCenterDashboard();
	manageCenterDashboard.validateOrganizations();
}
	@AfterMethod
	public void close()
	{
		driver.quit();
	}
}
