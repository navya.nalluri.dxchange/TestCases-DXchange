package com.dxchangeio.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dxchangeio.Base.BaseTest;
import com.dxchangeio.pages.LoginPage;
import com.dxchangeio.pages.SettingsPage;

public class SettingsPageTest extends BaseTest{
	public static LoginPage loginPage;
	public static SettingsPage settingsPage;
	@BeforeMethod
	public void setUp()
	{
		initialize();
		openBrowser();
		loginPage = new LoginPage();
		System.out.println(prop.getProperty("browser"));
		System.out.println(prop.getProperty("email"));
		loginPage.validateLogin(prop.getProperty("email"), prop.getProperty("password"));
		System.out.println("Test passed");
	}
	@Test
	public void verifySettingsLink()
	{
		settingsPage=new SettingsPage();
		settingsPage.moveToSettingLink();
		System.out.println("SettingsLink is Displayed");
	}
	@AfterMethod
	public void close()
	{
		driver.quit();
	}
	
	

}
