package com.dxchangeio.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dxchangeio.Base.BaseTest;
import com.dxchangeio.pages.LoginPage;
import com.dxchangeio.pages.SettingsPage;
import com.dxchangeio.pages.SettingsUserPrefrencesPage;

  public class SettingsUserPreferencesPageTest extends BaseTest{
//	public static SettingsUserPrefrencesPage settingsUserPrefrencesPage;
	public static LoginPage loginPage;
	public static SettingsPage settingsPage;
	@BeforeMethod
	public void setUp()
	{
		initialize();
		openBrowser();
		loginPage = new LoginPage();
		loginPage.validateLogin(prop.getProperty("email"), prop.getProperty("password"));
		System.out.println("Test passed");
		settingsPage = new SettingsPage();
		settingsPage.moveToSettingLink();
		System.out.println("SettingsLink is Displayed");
	}
	@Test
	public void verifyUserPrefrencesDetails() throws InterruptedException
	{
		System.out.println("verify");
		SettingsUserPrefrencesPage settingsUserPrefrencesPage = new SettingsUserPrefrencesPage();
		System.out.println("object created");
		settingsUserPrefrencesPage.validateEnivornment();
	}
	@AfterMethod
	public void close()
	{
		driver.quit();
	}

}
