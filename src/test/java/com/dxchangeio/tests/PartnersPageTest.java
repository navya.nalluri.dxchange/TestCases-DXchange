package com.dxchangeio.tests;

import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dxchangeio.Base.BaseTest;
import com.dxchangeio.pages.DesignCenterPage;
import com.dxchangeio.pages.HomePage;
import com.dxchangeio.pages.LoginPage;
import com.dxchangeio.pages.PartnersPage;

public class PartnersPageTest extends BaseTest{
	public static LoginPage loginPage;
	public static HomePage homePage;
	public static DesignCenterPage designCenterPage;
	public static PartnersPage partnersPage;
	
	@BeforeMethod
	public void setUp()
	{
		initialize();
		openBrowser();
		loginPage=new LoginPage();
		loginPage.validateLogin(prop.getProperty("email"), prop.getProperty("password"));
		homePage= new HomePage();
		homePage.verifyDesignCenterTxt();
		designCenterPage= new DesignCenterPage();
		designCenterPage.verifyPartnersLink();
		
	}
	@Test
	public void verifycreateBtn() throws InterruptedException
	{
		partnersPage = new PartnersPage();
		partnersPage.validatecreateBtn();
	}

	
}
