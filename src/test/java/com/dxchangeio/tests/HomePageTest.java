package com.dxchangeio.tests;

import org.testng.Assert;
import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dxchangeio.Base.BaseTest;
import com.dxchangeio.Util.UtilTest;
import com.dxchangeio.pages.HomePage;
import com.dxchangeio.pages.LoginPage;

public class HomePageTest extends BaseTest {
	public static HomePage homePage;
	public static LoginPage loginPage;
public static UtilTest utilTest;
	@BeforeMethod
	public void setUp() {
		initialize();
		utilTest= new UtilTest();
		openBrowser();
		loginPage = new LoginPage();
		System.out.println(prop.getProperty("browser"));
		System.out.println(prop.getProperty("email"));
		loginPage.validateLogin(prop.getProperty("email"), prop.getProperty("password"));
		System.out.println("Test passed");
	}

	@Test(priority = 1)
	public void verifyHomePageTitle() throws InterruptedException {
		homePage = new HomePage();
		String homePageTitle = homePage.getHomePageTitle();
		Assert.assertEquals(homePageTitle, "DXchange Platform");
		System.out.println("Successfully Title is Displayed");
	}

	@Test(priority  = 2)
	public void validateDesignCenter() {
		homePage = new HomePage();
		homePage.verifyDesignCenterTxt();
		
	}
	
	@Test(priority = 3)
	public void verifyManageCenterDB() {
		homePage = new HomePage();
		homePage.validateManageCenter();
	}

	@AfterMethod
	public void close() {
		driver.quit();
	}
}
