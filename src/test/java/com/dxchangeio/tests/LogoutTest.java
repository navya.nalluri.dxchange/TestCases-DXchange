package com.dxchangeio.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dxchangeio.Base.BaseTest;
import com.dxchangeio.pages.LoginPage;
import com.dxchangeio.pages.LogoutPage;

public class LogoutTest extends BaseTest {
	 LogoutPage logoutPage;
	 LoginPage loginPage;

	@BeforeMethod
	public void setUp() {
		initialize();
		openBrowser();
		loginPage = new LoginPage();
		System.out.println(prop.getProperty("browser"));
		System.out.println(prop.getProperty("email"));
		loginPage.validateLogin(prop.getProperty("email"), prop.getProperty("password"));
		System.out.println("Test passed");
	}

	@Test
	public void verifyLogoutLink() {
		logoutPage = new LogoutPage();
		logoutPage.moveToHarishLink();
		System.out.println("Successfully Loggedout");
	}

	
	  @AfterMethod 
	  public void close() { 
	  driver.quit(); }
	 
}
