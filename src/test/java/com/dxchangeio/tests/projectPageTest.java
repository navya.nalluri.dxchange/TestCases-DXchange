package com.dxchangeio.tests;

import org.testng.annotations.AfterMethod;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import com.dxchangeio.Base.BaseTest;
import com.dxchangeio.pages.DesignCenterPage;
import com.dxchangeio.pages.HomePage;
import com.dxchangeio.pages.LoginPage;
import com.dxchangeio.pages.ProjectPage;

public class projectPageTest extends BaseTest{
	public static LoginPage loginPage;
	public static ProjectPage projectPage;
	public static DesignCenterPage designCenterPage;
	public static HomePage homePage;
	@BeforeMethod
	public void setUp() throws InterruptedException
	{
		initialize();
		openBrowser();
		loginPage =new LoginPage();
		loginPage.validateLogin(prop.getProperty("email"),prop.getProperty("password"));
		System.out.println("Test passed");
		homePage = new HomePage();
		homePage.verifyDesignCenterTxt();
		/*System.out.println("Successfully entered to Design Center Platform");*/
		designCenterPage=new DesignCenterPage();
		designCenterPage.verifyProjectsLink();
		/*System.out.println("DesignCenter platform Displayed successfully");*/
		
	}
	@Test
	public void verifyHomeLink() throws InterruptedException
	{
		projectPage=new ProjectPage();
		projectPage.validateHomeLink();
		
	}
	@AfterMethod
	public void close()
	{
		driver.quit();
	}

}
